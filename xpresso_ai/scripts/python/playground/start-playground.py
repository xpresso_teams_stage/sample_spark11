"""
This script manages the xpresso environment locally.
"""
import fcntl
import os
import sys
import threading
import time
import docker
import subprocess


class XpressoDevelopmentEnvironment:
    """ This class manages the development environment for the users locally.
    It is responsible for :

    1. Start the docker container
    2. Commit the docker container on exit
    3. Push the docker image
    4. Ability to Reproduce the docker image (In-Progress)
    """

    USERNAME = "admin"
    PASSWORD = "Abz00ba@123"
    HOSTNAME = "xpresso"

    DOCKER_NAME = "xpresso_devenv"
    DOCKER_REGISTRY = "dockerregistry.xpresso.ai"
    DOCKER_IMAGE = "dockerregistry.xpresso.ai/library/xpresso_local:2.0.9.rc"
    NETWORK = "host"

    APP_FOLDER = "/app"

    def __init__(self, project_name: str, ):
        if os.geteuid() != 0:
            print("Error: Script must be run as root. Use sudo")
            sys.exit(1)
        self.client = docker.from_env()
        self.client.login(username="admin",
                          password="Abz00ba@123",
                          registry="dockerregistry.xpresso.ai")
        self.additional_volume = {}
        self.container_name = f"{project_name}_{self.DOCKER_NAME}"
        self.image_name = f"{self.DOCKER_REGISTRY}/{project_name}/xpresso_local_env:1.0"
        self.host_name = f"{project_name}_{self.HOSTNAME}"

    def start(self):
        # Remove existing container
        try:
            curr_cont = self.client.containers.get(container_id=self.container_name)
            should_stop = input(f"You already have a container with name {self.container_name} "
                  f"running.\nDo you want to shut down the existing container "
                          f"and start a new instance[y/n]: ")
            if should_stop.lower() != "y":
                print("Nothing left to do. Exiting...")
                sys.exit(0)
            print(f"Removing the container: {self.container_name}")
            curr_cont.stop()
            curr_cont.remove()  
            time.sleep(20)
        except( docker.errors.NotFound, docker.errors.APIError):
            # This is expected. Nothing needs to be done
            pass
        self.pull_base_image()
        try:
            current_directory = os.getcwd()
            volumes = {
                current_directory: self.APP_FOLDER
            }
            volumes.update(self.additional_volume)

            print("Booting up the environment")
            cmd = f"/usr/bin/docker run -it " \
                  f"--hostname={self.host_name} " \
                  f"--name {self.container_name} " \
                  f"--net=host"

            for src, dest in volumes.items():
                cmd = f"{cmd} -v {src}:{dest}"
            cmd = f"{cmd} {self.image_name}"
            self.run_interactive_shell(cmd)
            print("Saving the environment")
        except docker.errors.ImageNotFound:
            print(f"Docker image: {self.image_name} not found. Download the base"
                  f"image from the xpresso registry")
            sys.exit(1)
        except docker.errors.APIError as api_error:
            print(f"Failed to start the environment: {str(api_error)}")
            sys.exit(1)
        except docker.errors.ContainerError:
            print("Exiting the container...")
        self.commit(container_name=self.container_name)

    @staticmethod
    def run_interactive_shell(cmd: str):

        print(cmd.split(' '))
        print(cmd)
        proc = subprocess.Popen(cmd.split(' '),
                                stdin=subprocess.PIPE,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE)

        # To avoid deadlocks: careful to: add \n to output, flush output, use
        # readline() rather than read()
        proc.stdin.write(b'2+2\n')
        proc.stdin.flush()
        print(proc.stdout.readline())

        proc.stdin.close()
        proc.terminate()
        proc.wait(timeout=0.2)



    def pull_base_image(self):
        try:
            image_name = self.client.images.get(name=self.DOCKER_IMAGE)
            image_name.tag(repository=self.image_name)
            is_image_already_exist = True
        except docker.errors.ImageNotFound:
            is_image_already_exist = False
            pass

        if not is_image_already_exist:
            try:
                self.client.images.pull(repository=self.DOCKER_IMAGE,
                                        tag=self.image_name)
            except docker.errors.APIError as api_error:
                print(f"Failed to pull the image: {str(api_error)}")
                sys.exit(1)

    def commit(self, container_name: str):
        """ Commit the docker container for later use """
        print("Committing the changes made...")
        try:
            current_image_name = self.client.images.get(name=self.image_name)
            current_image_id = current_image_name.id
        except docker.errors.ImageNotFound:
            current_image_id = None

        try:
            last_used_container = self.client.containers.get(container_id=container_name)
            last_used_container.commit(repository=self.DOCKER_REGISTRY,
                                       tag=self.image_name,
                                       author="Xpresso Operation",
                                       message="Auto commit the code changes")
            print(f"Commit successful: {self.image_name}")
            print(f"You can now use this as your base image during build.\n"
                  f"Simply use the {self.image_name} in the FROM tak in "
                  f"thd Dockerfile (xprbuild/docker/Dockerfile)")

        except docker.errors.NotFound:
            print("Commit Failed. No container found to commit.")
            return
        except docker.errors.APIError as err:
            print(f"Commit Failed. {str(err)}")

        if not current_image_id:
            print("Cleanup completed")
            return

        # Cleaning up the existing images
        try:
            print("Cleaning old data")
            self.client.images.remove(name=current_image_id)
        except (docker.errors.ImageNotFound,
                docker.errors.APIError):
            # Nothing left to do
            return

    def push(self):
        try:
            self.client.images.push(repository=self.image_name)
        except docker.errors.APIError as err:
            print(f"Docker push Failed: {err}")
            sys.exit(1)


if __name__ == "__main__":
    if len(sys.argv) < 3:
        print("Error: Please provide the command: "
              "python start-xpresso <project_name> <start/push>")
        sys.exit(1)

    project = sys.argv[1]
    command = sys.argv[2]
    xpresso = XpressoDevelopmentEnvironment(project_name=project)
    if command == "start":
        xpresso.start()
    elif command == "push":
        xpresso.push()
    else:
        print("Error: Invalid command provided. Please provide start/push")
