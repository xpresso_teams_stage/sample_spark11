#! /bin/bash

current_directory=$PWD

# This prints the welcome message
/bin/bash /opt/xpresso.ai/scripts/python/playground/welcome-message.sh

# Create the virtual environment if not present
cd ${current_directory}
if [[ ! -d ${current_directory}/vxpr ]]
then
  python -m venv vxpr --system-site-packages
fi

. ${current_directory}/vxpr/bin/activate

exec "$@"